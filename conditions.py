print not (1 == 1 and 0 != 1)
print not (True and False)
print 1 == 1 and (not ("testing" == 1 or 1 == 0))
print "chunky" == "bacon" and (not (3 == 4 or 3 == 3))

# bit
print False and 1
print True and 1
print "test" and "test"
print 1 and 1

people = 20
cats = 30
dogs = 15


if people < cats:
    print "Too many cats! %s" %cats

if people < dogs:
    print "Dogs ! %s" %dogs

if people > dogs:
	print "Not many dogs! %s" %people

people = 30
cars = 40
trucks = 15

if cars > people:
    print "We should take the cars."
elif cars < people:
    print "We should not take the cars."
else:
    print "We can't decide."

if 1 <= people <= 30:
	print "People less than 31"
if not (people in range(1, 10)):
	print "People not in range than 1-10"


from pprint import pprint

change = [1, 'pennies', 2, 'dimes', 3, 'quarters']
list_2d = [[1,2,3],[4,5,6]]

# also we can go through mixed lists too
# notice we have to use %r since we don't know what's in it
for i in change:
    print "I got %r" % i

for list in list_2d:
    print list

print '-' * 50
pprint(list_2d)

print '-' * 50
pprint(locals())


print '-' * 50
class stdClass(object): pass
obj=stdClass()
obj.int=1
obj.tup=(1,2,3,4)
obj.dict={'a':1,'b':2, 'c':3, 'more':{'z':26,'y':25}}
obj.list=[1,2,3,'a','b','c',[1,2,3,4]]
obj.subObj=stdClass()
obj.subObj.value='foobar'
print type(obj)


#https://docs.python.org/2/library/pdb.html


x1 = 5
y1 = 5
x2 = 'Hello'
y2 = 'Hello'
x3 = [1,2,3]
y3 = [1,2,3]

# Output: False
print(x1 is not y1)

# Output: True
print(x2 is y2)

# Output: False
# But x3 and y3 are list. They are equal but not identical. 
# Since list are mutable (can be changed),
# thong dich interpreter locates them separately in memory although they are equal.
print(x3 is y3)

print "-" * 50

x = 'Hello world'
y = {1:'a',2:'b'}

# Output: True
print('H' in x)

# Output: True
print('hello' not in x)

# Output: True
print(1 in y)

# Output: False
print('a' in y)


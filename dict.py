stuff = {1: '1', 'name': 'Zed', 'age': 39, 'height': 6 * 12 + 2}
print stuff['name']
print stuff['height']
stuff['city'] = "San Francisco"
print stuff

del stuff[1]
print stuff

print '-' * 50

# create a mapping of state to abbreviation
states = {
    'Oregon': 'MI',
    'Florida': 'FL',
    'California': 'CA'
}

# create a basic set of states and some cities in them
cities = {
    'CA': 'San Francisco',
    'MI': 'Detroit',
    'FL': 'Jacksonville'
}

for state, abbrev in states.items():
    print "%s state is abbreviated %s and has city %s" % (
        state, abbrev, cities[abbrev])

city = cities.get('TX', 'Does Not Exist')
print "The city for the state 'TX' is: %s" % city

cities['TX'] = 'testtest'; 
city = cities.get('TX', 'Does Not Exist')
print "The city for the state 'TX' is: %s" % city



# mystuff['apple'] # get apple from dict
# mystuff.apple() # get apple from the module
# mystuff.tangerine # same thing, it's just a variable

from sys import argv

script, filename = argv
txt = open(filename)

print "Your file %r:" %filename
print txt.read()

print "We're going to erase %r." % filename
print "If you don't want that, hit CTRL-C (^C)."
print "If you do want that, hit RETURN."

raw_input("?")

target = open(filename, 'w')

print "Truncating the file.  Goodbye!"
target.truncate()

txt.close()

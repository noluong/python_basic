a = {5,2,3,1,4}

# printing set variable
print("a = ", a)

# data type of variable a
print(type(a))

a = {1,2,2,3,3,3}
print a

#Since, set are unordered collection, indexing has no meaning. Hence the slicing operator [] does not work.
print a[1]



# -*- coding: utf-8 -*-

print(1,2,3,4)
# Output: 1 2 3 4

print "-" * 50

x = 5; y = 10
print('The value of x is {} and y is {}'.format(x,y))

print "-" * 50

print('I love {0} and {1}'.format('bread','butter'))
# Output: I love bread and butter

print "-" * 50

my_name = 'Nola'
my_age = 26
my_eyes = 'Black'
my_hair = 'Yellow'

my_code = "123.8"
w = "This is the left side of..."
e = "a string with a right side."

print w + e
print my_code
print "test %r" % my_code
print "-" * 50

print "はじめた"
print "Let talk about %r" %my_name
print "She's got %s eyes and %d age." % (my_eyes, my_age)

print "-" * 50

days = "Mon Tue Wed Thu Fri Sat Sun"
months = "Jan\nFeb\nMar\nApr\nMay\nJun\nJul\nAug"

print "Here are the days: ", days
print "Here are the months: ", months

print """
There's something going on here.
With the three double-quotes.
We'll be able to type as much as we like.
Even 4 lines if we want, or 5, or 6.
"""



